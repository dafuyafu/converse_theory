\documentclass[a4j, twoside]{jsarticle}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{theorem}
\usepackage{ascmac}
\usepackage[dvipdfmx]{graphicx}
\usepackage{fancyhdr}

% proof environment for theorem.sty
\makeatletter
\newenvironment{prf}[1][\proofname]{\par
  \normalfont
  \topsep6\p@\@plus6\p@ \trivlist
  \item[\hskip\labelsep{\bfseries #1}\@addpunct{\bfseries.}]\ignorespaces
}{
  \endtrivlist
}
\newcommand{\proofname}{証明}
\makeatother

% theorem environments
\theorembodyfont{\upshape}
\newtheorem{theo}{定理}[section]
\newtheorem{coro}[theo]{系}
\newtheorem{lemm}[theo]{補題}
\newtheorem{prop}[theo]{命題}
\newtheorem{defi}[theo]{定義}
\newtheorem{exam}[theo]{例}
\newtheorem{remk}[theo]{注意}

% universal newcommands
\newcommand{\Qed}{\quad $\square$}
\newcommand{\nn}{\mathbb{N}}
\newcommand{\zz}{\mathbb{Z}}
\newcommand{\qq}{\mathbb{Q}}
\newcommand{\rr}{\mathbb{R}}
\newcommand{\cc}{\mathbb{C}}
\newcommand{\pp}{\mathbb{P}}
\newcommand{\rIn}{\rotatebox{90}{$\in$}}
\newcommand{\lk}{\left\{}
\newcommand{\rk}{\right\}}
\newcommand{\lkk}{\left[}
\newcommand{\rkk}{\right]}
\newcommand{\Ker}{\mathrm{Ker}\,}
\newcommand{\Img}{\mathrm{Im}\,}
\newcommand{\Coker}{\mathrm{Coker}\,}
\newcommand{\Prf}{\textbf{証明}.\ }
\newcommand{\rst}{\!\mid}

% temporary newcommands
\newcommand{\Ob}[1]{\mathrm{Ob}(#1)}
\newcommand{\Rp}[1]{\mathrm{Rp}(#1)}
\newcommand{\Rf}[1]{\mathrm{Rf}(#1)}
\newcommand{\Pro}[1]{\mathrm{Pro}(#1)}
\newcommand{\Fun}[1]{\mathrm{Fun}(#1)}
\newcommand{\Zah}{\mathbb{Z}}
\newcommand{\Rel}{\mathbb{R}}

% contents
\begin{document}
\title{逆学入門}
\author{Sid Montague}
\maketitle
\thispagestyle{fancy}
\fancyhead{}
\fancyfoot{}

% header style
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}
\fancyhead{}
\fancyfoot{}
\begin{abstract}
逆学とは集合における元同士の関係性を超局所的な観点から考察する数学の分野である．現代の逆学は定義空間論に基づいており，まず始めに定義空間について学習する．定義空間の主要なテーマは極大完備成分と被覆である．これは定義空間を分類および完備化する際に重要になる概念である．さらに，元同士を抽象的かつ超局所的に議論するための言語である半同値関係について基本的な内容を学習し，その知識を使って有限定義空間の分類，定義空間の完備化を学習する．次に代数的構造を保つような定義空間の一般化を与え，その知識を統合して代数学への応用を研究する．
\end{abstract}
\tableofcontents
\newpage

\fancyhead[RO,LE]{\thepage}
\fancyhead[CE]{Sid Montague}
\fancyhead[CO]{逆学入門}

\section{定義空間}
逆学の原始的な動機はその名の通り，ある集合上の命題の逆がどのような場合に成り立つかを検証することであった．現在ではその形式的な意味合いは大きく変化し，命題を介さず直接部分集合を対応させる定義に変化したが，もととなる考え方は等価である．定義空間の知識を用いて代数構造を別角度から理解することがこの章の目的である．以下この章では$X$を任意の集合とする．
\subsection{定義空間と射}
\begin{defi}
$X$から$X$のべき集合への写像$\alpha : X \to 2^X$が任意の$x \in X$について$x \in \alpha(x)$であるとき，これを\textbf{類似系}(similarity system)という．台集合$X$と類似系$\alpha$の組$X=(X,\alpha)$を$X$上の\textbf{定義空間}(definition space)という．
\end{defi}

次に，いろいろな定義空間の例を見ていく．
\begin{exam}
\label{trivial_space}
単集合上の定義空間$(\lk a \rk, \alpha)$を自明な(trivial)定義空間という．自明な定義空間の類似系は単集合の元を$a$とすると$\alpha: a \mapsto \lk a \rk$である．
\end{exam}
\begin{exam}
定義空間$X = (X, \alpha)$の類似系を$\alpha(x) = X$とする．このような定義空間を密着定義空間，あるいは単に密着空間という．
\end{exam}
\begin{exam}
\label{oddeven}
$\Zah$上の定義空間$X$を
\[
\alpha(x) :=
\begin{cases}
\lk \ldots, -1, 1, 3, \ldots \rk & (x = 2m-1)\\
\lk \ldots, -2, 0, 2, \ldots \rk & (x = 2m) 
\end{cases}
\]
と定めると，$X=(\Zah,\alpha)$は定義空間を為す．
\end{exam}
\begin{defi}
$X=(X,\alpha)$，$Y=(Y,\beta)$をそれぞれ定義空間とする．この時，定義空間の\textbf{射}(morphism)とは，定義空間の写像$f:X\to Y$であって，$f \circ \alpha = \beta \circ f$を満たすものである．つまり，
\[
\begin{array}{ccc}
X & \xrightarrow{f}  & Y\\
\alpha \downarrow & & \downarrow \beta \\
2^X & \xrightarrow{f} & 2^Y
\end{array}
\]
という図式が可換になるものである．また，全単射な射が存在するとき，$X$と$Y$は\textbf{同型}(isomorphic)であるといい，$X \simeq Y$と表す．また，そのような射を\textbf{同型射}(isomorphism)という．
\end{defi}
\begin{exam}
\label{morphism1}
定義空間$X=(X,\alpha)$と$Y=(Y,\beta)$について，$X = \lk 1, 2 \rk$を
\begin{itemize}
\item $\alpha(1) = \lk 1 \rk$
\item $\alpha(2) = \lk 2 \rk$
\end{itemize}
とし，$Y = \lk 1, 2, 3 \rk$を
\begin{itemize}
\item $\beta(1) = \lk 1 \rk$
\item $\beta(2) = \lk 2 \rk$
\item $\beta(3) = \lk 3 \rk$
\end{itemize}
とする．このとき$f:X\to Y$(ただし$f(n) = n$)は射になる．
\end{exam}
\begin{exam}
$\Zah$上の定義空間$X=(X,\alpha)$と$Y=(Y,\beta)$について，$X = \lk 1, 2, 3 \rk$を
\begin{itemize}
\item $\alpha(1) = \lk 1, 3 \rk$
\item $\alpha(2) = \lk 2 \rk$
\item $\alpha(3) = \lk 1, 3 \rk$
\end{itemize}
とし，$Y = \lk 1, 2 \rk$を
\begin{itemize}
\item $\beta(1) = \lk 1 \rk$
\item $\beta(2) = \lk 2 \rk$
\end{itemize}
とすると，$f:X\to Y$(ただし$f(1) = f(3) = 1$，$f(2) = 2$)は射になる．実際，$2$については$(\ref{morphism1})$と同じ結果を得る．$1,3$については，
\[
\begin{array}{cc}
\begin{array}{rl}
f \circ \alpha \ (1) &= f(\lk 1, 3 \rk) \\
&= \lk 1 \rk \\
\beta \circ f \ (1) &= \beta (1)\\
&= \lk 1 \rk
\end{array} &
\begin{array}{rl}
f \circ \alpha \ (3) &= f(\lk 1, 3 \rk) \\
&= \lk 1 \rk \\
\beta \circ f \ (3) &= \beta (1)\\
&= \lk 1 \rk
\end{array}
\end{array}
\]
となり，$f \circ \alpha = \beta \circ f$が成り立つ．
\end{exam}
\subsection{部分定義空間と完備成分}
\begin{defi}
定義空間$X = (X, \alpha)$について，$X'$を$X$の部分集合，$\alpha'$を
\[
\begin{array}{rccc}
\alpha':& X' & \to & 2^{X'}\\
& x & \mapsto & \alpha(x) \cap X'
\end{array}
\]
と定めた時，それらのなす定義空間$X' = (X', \alpha')$を$X$の\textbf{部分定義空間}(subdefinition space)，あるいは単に\textbf{部分空間}(subspace)という．
\end{defi}
\begin{defi}
定義空間$X = (X, \alpha)$において，$\alpha(x) = \lk x\rk$である時，$x$は$X$において\textbf{完備}(complete)であるという．また，定義空間が\textbf{完備}であるとは，任意の元が完備であることをいう．
\end{defi}
\begin{exam}
自明な定義空間は完備である．
\end{exam}
\begin{defi}
任意の定義空間は完備な部分空間を含む．これを\textbf{完備成分}(complete component)という．また，定義空間$X$の完備成分$\mathfrak{m} \neq X$について，$\mathfrak{m} \subsetneq \mathfrak{a} \subsetneq X$を満たす完備成分$\mathfrak{a}$が存在しないとき，$\mathfrak{m}$は\textbf{極大}(maximal)であるという．
\end{defi}
\begin{exam}
任意の定義空間について，その任意の部分シングルトンはその定義空間の完備成分である．また，極大の逆として，定義空間全体ではなく完備成分を真の部分集合として持たない完備成分を極小(minimal)であるとすると，定義空間が自明でない限りそれは先述のシングルトンである．つまり，任意の非自明な定義空間はそれ自体が自明な定義空間であるような極小完備成分を部分空間として持つ．
\end{exam}
\begin{exam}
$X$を$(\ref{oddeven})$の定義空間とする．この時，前述の通りシングルトンは$X$の完備成分である．また，$\left\{ 1, 2 \right\}$のように奇数と偶数を一つずつ選べば，これも完備成分である．しかし，3つ以上の元を持つような完備成分は存在しないので，$\left\{ 1, 2 \right\}$は$X$の極大完備成分の1つである．
\end{exam}
\begin{exam}
完備定義空間$X$について，全ての部分空間は完備成分であり，そのうち極大完備成分は全体から一点を除いた部分空間$X - \left\{ x \right\}$である．
\end{exam}
\begin{theo}[モンターニュの定理]
非自明な定義空間は少なくとも$1$つ極大完備成分を持つ．
\end{theo}
\begin{prf}
定義空間$X$について，$C$を$X$の$X$自身でない完備成分全体の集合とする．$C$に通常の包含関係で順序を入れると$C$は半順序集合となる．さらに，$X$は非自明なので$C$は空でない．$(\mathfrak{a}_i)$を$C$の鎖とする．各$\mathfrak{a}_i$は$\mathfrak{a}_i= (\mathfrak{a}_i, \alpha_i)$という$X$の完備成分である．ここで，$\mathfrak{a} = \bigcup_i \mathfrak{a}_i$とする．\\  
\begin{itemize}
\item $\mathfrak{a} \neq X$\\
任意の$i,j$について$\mathfrak{a}_i \subseteq \mathfrak{a}_j$あるいは$\mathfrak{a}_j \subseteq \mathfrak{a}_i$であるので，$\mathfrak{a} = D$のとき，ある$l$で$\mathfrak{a}_l = D$となるが，$\mathfrak{a}_l \not\in C$より矛盾．
\item $\mathfrak{a}$は完備\\  
$\mathfrak{a}$は$D$の部分空間であり，$\mathfrak{a}=(\mathfrak{a}, \alpha)$と表せる．ここで，ある$x,y \in \mathfrak{a}$が存在して，$\alpha(x) = \left\{ x, y \right\}$と仮定する．このとき，ある$i$で$x \in \mathfrak{a}_i$となるが，  
\begin{itemize}
\item $y \in \mathfrak{a}_i$のとき，$\alpha_i (x) = \left\{ x, y \right\}$となり，$\mathfrak{a}_i$が完備成分であることに矛盾する．  
\item $y \in \mathfrak{a}_j\ \ \mathrm{s.t.}\ \ \mathfrak{a}_i \subset \mathfrak{a}_j$のとき，$\alpha_j(x) = \left\{ x, y \right\}$となり，$\mathfrak{a}_j$が完備成分であることに矛盾する．  
\end{itemize}
\end{itemize}
また，明らかに任意の$\mathfrak{a}_j \in (\mathfrak{a}_i)$について$\mathfrak{a}_j \subseteq \mathfrak{a}$であるので，$\mathfrak{a}$は鎖$(\mathfrak{a}_i)$の上界である．以上より，半順序集合$C$は任意の鎖が$C$に上界を持つので，ツォルンの補題より$C$は極大元をもつ．\Qed
\end{prf}

\subsection{一意分解空間}
\begin{defi}
定義空間$X = (X, \alpha)$の部分集合$\left\{ c_i \right\}$について$X = \bigcup_i \alpha(c_i)$であるとき，$\left\{ c_i \right\}$を$X$の\textbf{被覆}(cover)という．また，定義空間$X$の被覆$C = \left\{ c_i \right\}$について，その部分集合$D = \left\{ d_i \right\} \subset C$であって，それもまた$X$の被覆であるとき，$D$を$C$の\textbf{部分被覆}(subcover)という．
\end{defi}
\begin{defi}
定義空間の任意の被覆が有限の部分被覆を持つとき，その定義空間は\textbf{コンパクト}(compact)であるという．
\end{defi}
\begin{exam}
任意の有限定義空間はコンパクトである．
\end{exam}
\begin{exam}
$X$を$(\ref{oddeven})$の定義空間とすると，$X$のコンパクトである．実際，偶数と奇数をそれぞれ１つずつ含むような部分集合は被覆になるが，そのうち偶数と奇数を１つずつとれば有限の部分被覆となる．
\end{exam}
\begin{coro}
自由空間がコンパクトであることと，その自由空間が有限であることは同値である．
\end{coro}
\begin{defi}
定義空間$X$とその極大完備成分の族$\lk \mathfrak{m}_i \rk$について，$X = \bigcup_i \mathfrak{m}_i$であって，そのような$\lk \mathfrak{m}_i \rk$が一意的に定まる場合，$X$は\textbf{一意分解定義空間}(unique factrization definition space)である，\textbf{一意分解空間}である，あるいは一意分解的であるという．
\end{defi}
\begin{exam}
密着定義空間は極大完備成分であるシングルトンを用いて一意的に分解できるが，完備定義空間は一意分解的でない．なぜならば，任意の2つの相異なる極大完備成分に分解できるからである．
\end{exam}
\begin{exam}
\label{ufspace1}
定義空間$X=(\lk a,b,c \rk, \alpha)$を
\begin{align*}
\alpha(a) &= \lk a,b,c \rk \\
\alpha(b) &= \lk b \rk \\
\alpha(c) &= \lk c \rk
\end{align*}
と定める．このとき，$\lk a \rk$と$\lk b, c \rk$はそれぞれ$X$の極大完備成分であり，この場合はそれら以外に極大完備成分を持たない．更に，$X = \lk a \rk \cup \lk b, c \rk$であるので，$X$は一意分解的である．
\end{exam}
% \begin{lemm}
% 一意分解空間$D$がその極大完備成分の族$\lk \mathfrak{m}_i \rk$によって$D = \bigcup_i \mathfrak{m}_i$と分解されるならば，$\lk \mathfrak{m}_i \rk$は$D$の極大完備成分全体である．
% \end{lemm}
% \begin{prf}
% ある極大完備成分$\mathfrak{m}' \subset D$が存在して，$\lk \mathfrak{m}_i \rk$が$\mathfrak{m}'$を含まないと仮定する．
% \end{prf}

\section{半同値}
半同値関係とは同値関係の満たす条件のうち，反射律と推移率を除いたもの，つまり対称律のみを満たす関係である．半同値関係はそれ自体が興味深い研究対象であるが，逆学においては次章の有限定義空間の分類においてその言葉が必要となる．ここでは半同値関係の基本的な性質や最小限の知識を記述する．
\subsection{半同値関係}
$X$を任意の集合とする．$X$上の二項関係とは，直積集合$X \times X$の部分集合
\begin{defi}
$X$上の二項関係$R$について$(x,y) \in R$ならば$(y,x) \in R$であるとき，$R$は\textbf{半同値関係}(semiequivalence relation)という．$(x,y) \in R$であるとき，$x$と$y$は\textbf{半同値}(semiequivalent)であるといい，$x \smile_R y$あるいは紛れのない場合$x \smile y$と表す．
\end{defi}
\begin{defi}
$X$上の半同値関係$R$について，ある$x\in X$が$(x,x) \in R$であるとき，$x$は$R$において\textbf{反射的}(reflexive)であるという．また，$X$上の半同値関係$R$が反射的であるとは，$R$によって半同値が付けられている元全てが反射的であることをいう．つまり，全ての$(x,y) \in R$について$(x,x) \in R$かつ$(y,y) \in R$であるとき，$R$は反射的であるという．
\end{defi}
\begin{exam}
\label{tonari}
$\Zah$上の二項関係$R_{\Zah}$を
\[
R_{\Zah} = \left\{ (n,m) \mid m - 1 \leq n \leq m + 1 \right\}
\]
とすると，$R_{\Zah}$は反射的な半同値関係である．実際，$(a,b) \in R_{\Zah}$とすると，$a = b+1$のときは$b=a-1$より$(b,a) \in R_{\Zah}$，$a = b-1$のときは$b=a+1$より$(b,a) \in R_{\Zah}$である．
\end{exam}
\begin{exam}
$\Zah$上の二項関係$R$を
\[
R = \left\{ (n,m) \mid \mathrm{GCD}(n,m) = 1 \right\}
\]
とすると，$R$は半同値関係であるが，反射的でない．
\end{exam}
\begin{exam}
一般に，全ての同値関係は半同値関係である．逆は必ずしも成り立たない．
\end{exam}
\begin{remk}
$X$上の半同値関係$R$は$X$を頂点集合とするグラフである．一般にこのようなグラフはループや多重辺を含むが，反射的な元が存在しない場合ループは存在しない．また，そのような場合対称性から多重辺は双方向なので，それを一つの辺と見れば無向な単純グラフを得る．
\end{remk}
\begin{defi}
$X$上の点列を$\left\{ x_i \right\}$とする．ここで任意の$i$について$x_i \smile x_{i+1}$であるとき$\left\{ x_i \right\}$を\textbf{半同値列}(semiequivalence sequence)という．有限な半同値列について$\# \lk x_i \rk -1$を半同値列の\textbf{長さ}(length)といい，$\mathrm{length}(\lk x_i \rk)$と表す．
\end{defi}
\begin{exam}
$\Zah$に$(\ref{tonari})$の半同値関係を定める．この時，
\[
\cdots \smile -2 \smile -1 \smile 0 \smile 1 \smile 2 \smile \cdots
\]
という半同値列$\left\{ n_i \right\}$が存在する．
\end{exam}
以下，半同値列$\lk u_i \rk$について，ある$x\in X$が存在して$x \smile u_1 \smile u_2 \smile \cdots$である時，$x \smile \lk u_i \rk$と表す．また，有限な半同値列$\lk u_i \rk$についてある$x\in X$が存在して$\cdots \smile u_{n-1} \smile u_n \smile x$であるとき，$\lk u_i \rk \smile x$と表す．
\begin{defi}
$X$上に半同値関係$R$が定義されているとする．ここで，関数$d_R : X \times X \to \Rel \cup \lk \infty \rk$を以下のように定義する．
\begin{equation}
d_R(x,y) :=
\begin{cases}
0 & (x = y)\\
\mathrm{min}\lk \mathrm{length}(x \smile \lk u_i \rk \smile y) \rk & (x \neq y,\ \exists \lk u_i \rk_{i=1}^{n-1}) \\
\infty & (x\neq y,\ \not\exists \lk x \smile \lk u_i \rk \smile y \rk)
\end{cases}
\end{equation}
このとき$d_R$は超越距離関数となる．この$d_R$を$R$の\textbf{半同値距離}(semiequivalence distance)という．
\end{defi}
\begin{defi}
$X$上に半同値関係$R$が定義されているとする．ここで，二項関係$R^*$を以下のように定義する．
\[
(x,y) \in R^* \Leftrightarrow d_R(x,y) < \infty
\]
このとき$R^*$は同値関係となる．この$R^*$を$R$によって\textbf{誘導される}同値関係(induced equivalence relation)という．
\end{defi}
\begin{exam}
$\Zah$上に半同値関係$R = \lk (n,m) \mid n = m \pm 3 \rk$を定義すると，$R^* = \lk (n,m) \mid n \equiv m \ (\mathrm{mod}\ 3) \rk$という同値関係が誘導される．
\end{exam}

\subsection{位相空間の半同値}
位相空間が与えられた時，その位相構造からある半同値関係を定めることが出来る．開集合という局所的データからより\textit{局所的}なデータである点と点の関係性を記述するという方法を通じて位相空間を理解することが半同値関係と位相空間の対応を考える主たる動機である．
\begin{defi}
集合$X$上の半同値関係$R$と集合$Y$上の半同値関係$S$について，全単射$f:X \to Y$が存在して，任意の$x,y \in X$について$x \smile_R y \Leftrightarrow f(x) \smile_S f(y)$であるとき，$R$と$S$は\textbf{同型}(isomorphic)であるといい，$R \simeq S$と表す．
\end{defi}
以下，実際に位相構造から半同値関係を導くことを考える．(複数あることをいう．)
\begin{defi}
位相空間$X=(X,\mathcal{O})$に対し，二項関係$\mathcal{R}(X)$を以下のように定める．
\begin{equation}
x \smile y \Leftarrow
\begin{cases}
\exists \lk x \rk \in \mathcal{O} & (x = y)\\\
\exists \ U\ ( \neq X) \in \mathcal{O}\ \mathrm{s.t.}\ x \in U,\ y \in U & (x \neq y)
\end{cases}
\end{equation}
この時，$\mathcal{R}(X)$は半同値関係をなす．これを$X$の\textbf{双近傍半同値関係}(bineighbor semiequivalence relation)という．
\end{defi}
\begin{exam}
$X$が離散位相空間であるとき，$\mathcal{R}(X)$は同値関係をなす．また$X$が密着位相空間であるとき，$\mathcal{R}(X) = \varnothing$である．
\end{exam}
\begin{exam}
\label{abctopspace}
$X = \lk a, b, c \rk$について，開集合系を$\mathcal{O} = \lk \varnothing, \lk a \rk, \lk b,c \rk \rk$とすると，$X$の双近傍半同値$\mathcal{R}(X)$は
\[
\mathcal{R}(X) = \lk (a,a), (b,c), (c,b) \rk
\]
である．
\end{exam}
\begin{defi}
位相空間$X=(X,\mathcal{O})$に対し，二項関係$\mathcal{S}(X)$を以下のように定める．
\begin{equation}
x \smile y \Leftarrow \exists U, V \in \mathcal{O} \quad \mathrm{s.t.} \quad U \cap V = \varnothing,\ x \in U,\ y\in V 
\end{equation}
この時，$\mathcal{S}(X)$は半同値関係をなす．これを$X$の\textbf{分離半同値関係}(separation semiequivalence relation)という．
\end{defi}
\begin{exam}
$X$が離散位相空間であるとき，$\mathcal{S}(X)$は任意の異なる点の組を含む．また$X$が密着位相空間であるとき，$\mathcal{R}(X) = \varnothing$である．
\end{exam}
\begin{exam}
$X$を$(\ref{abctopspace})$の位相空間とすると，$X$の分離半同値関係$\mathcal{S}(X)$は，
\[
\mathcal{S}(X) = \lk (a,b), (b,a), (a,c), (c,a) \rk
\]
である．
\end{exam}
% 以上のように，位相空間の構造から半同値関係をwell-definedに定義することができる．この手続を圏論的に一般化するものが次の定義である．
% \begin{defi}
% 位相空間$X=(X,\mathcal{O})$に対し，二項関係$\mathcal{F}(X)$を定める．
% \end{defi}

% \section{有限定義空間の分類}
% この章では，主に有限定義空間をその特徴によって分類することを考える．そもそも，定義空間は位相空間の類似の概念である．というのは，各元について必ず近傍が存在すること，各元についてそれぞれ固有にラベルの付けられた\textit{開集合}が存在するという差異を除けば，空間の点と部分集合族の組として定義される定義空間はほとんど位相空間のようなものであると言っても過言ではない．位相空間が分離性やコンパクト性で分類されるのに対して，定義空間もまたそれらの類似の概念である半同値性や主体的コンパクト性などを用いて分類される．なお，本章は有限定義空間の分類が主たるテーマであるが，有限の場合における結果を一般の定義空間についても適用して議論する．
% \subsection{低位数の有限定義空間の例}
% \subsubsection{位数が1の定義空間}
% 位数$1$，つまり1つの実体からなる定義空間は$(\ref{trivial_space})$のように自明空間である．これは完備であり，ゆえに1つの実体からなる自由空間と定義空間として同型である．
% \subsubsection{位数が2以上の定義空間}
% \begin{exam}
% 位数が$2$のとき，定義空間$D = (D, \alpha)$は判別写像$\alpha$で分類できる． $D = \lk I_1, I_2 \rk$として，
% \begin{enumerate}
% \item $\alpha(I_1) = \alpha(I_2) = D$
% \item $\alpha(I_1) = \lk I_1 \rk, \qquad \alpha(I_2) = D$
% \item $\alpha(I_1) = D, \qquad \alpha(I_2) = \lk I_2 \rk$
% \item $\alpha(I_1) = \lk I_1 \rk, \qquad \alpha(I_2) = \lk I_2 \rk$
% \end{enumerate}
% の4つのパターンになる．1つめのパターンは位数2の密着空間であり，4つめのパターンは完備定義空間である．2つめと3つめのパターンは同型であるので，位数が2の時は同型を除いて3つに分類できる．
% \end{exam}
% \begin{exam}
% 位数が$3$のとき，定義空間$D = (D, \alpha)$は判別写像$\alpha$で分類できる． $D = \lk I_1, I_2, I_3 \rk$として，
% \begin{enumerate}
% \item $\alpha(I_1) = \alpha(I_2) = \alpha(I_3) = D$
% \item $\alpha(I_1) = \lk I_1 \rk, \quad \alpha(I_2) = D, \quad \alpha(I_3) = D$
% \item $\alpha(I_1) = \lk I_1, I_2 \rk, \quad \alpha(I_2) = D, \quad \alpha(I_3) = D$
% \item $\alpha(I_1) = D, \quad \alpha(I_2) = \lk I_2 \rk, \quad \alpha(I_3) = D$
% \item $\alpha(I_1) = D, \quad \alpha(I_2) = \lk I_1, I_2 \rk, \quad \alpha(I_3) = D$
% \item $\alpha(I_1) = D, \quad \alpha(I_2) = \lk I_2, I_3 \rk, \quad \alpha(I_3) = D$
% \item $\alpha(I_1) = D, \quad \alpha(I_2) = D, \quad \alpha(I_3) = \lk I_3 \rk$
% \item $\alpha(I_1) = D, \quad \alpha(I_2) = D, \quad \alpha(I_3) = \lk I_1, I_3 \rk$
% \item $\alpha(I_1) = D, \quad \alpha(I_2) = D, \quad \alpha(I_3) = \lk I_2, I_3 \rk$
% \item $\alpha(I_1) = D, \qquad \alpha(I_2) = \lk I_2 \rk$
% \item $\alpha(I_1) = \lk I_1 \rk, \qquad \alpha(I_2) = \lk I_2 \rk$
% \end{enumerate}
% brank.
% \end{exam}
% \subsection{有限定義空間}
% \subsection{定義空間の分類}

\section{定義空間の完備化}
この章では連続的な構造に対してどのように定義空間の知識を応用するかという目的のために定義空間の完備化という操作について記述する．定義空間の完備化は離散的なデータを持つ集合上ではほとんど直感的に構成できるが連続的なものにおいては必ずしもそうとは限らない．
\subsection{完備化}
\begin{defi}
$X = (X,\alpha)$を定義空間とする．ここで，ある$X^* = (X, \alpha^*)$が存在して
\begin{enumerate}
\item 任意の$x \in X$についてある$Y(x) \subseteq X$が存在して$\alpha^*(x) = \bigcap_{y_i \in Y(x)} \alpha(y_i)$
\item 任意の$x \in X$について$\alpha^*(x) = \lk x \rk$
\end{enumerate}
となるとき，$X^*$を$X$の\textbf{$D_1$-完備化}($D_1$-completion)という．また，そのような$X^*$が存在するとき$X$を\textbf{$D_1$-定義空間}($D_1$-definition space)，あるいは単に\textbf{$D_1$-空間}という．
\end{defi}
\begin{exam}
$X = \lk 1, 2, 3 \rk$上の定義空間$X = (X, \alpha)$について
\begin{align*}
\alpha(1) &= \lk 1,2 \rk\\
\alpha(2) &= \lk 2,3 \rk\\
\alpha(3) &= \lk 3,1 \rk
\end{align*}
とすると，$X$の元は全て完備でないが，$\alpha^*$をそれぞれ
\begin{align*}
\alpha^*(1) = \alpha(1) \cap \alpha(3) = \lk 1 \rk\\
\alpha^*(2) = \alpha(2) \cap \alpha(1) = \lk 2 \rk\\
\alpha^*(3) = \alpha(3) \cap \alpha(2) = \lk 3 \rk
\end{align*}
とすると，$X^* = (X,\alpha^*)$は完備定義空間となる．よって$X$は$D_1$-空間であり，その$D_1$-完備化は$X^*$である．
\end{exam}
\begin{exam}
\label{e_d2}
$X = \lk 1, 2, 3 \rk$上の定義空間$X = (X, \alpha)$について
\begin{align*}
\alpha(1) &= \lk 1 \rk\\
\alpha(2) &= \lk 1,2 \rk\\
\alpha(3) &= \lk 1,2,3 \rk
\end{align*}
とすると，$X$は完備ではない．しかし，任意の2つの元の類似の共通部分に$1$が含まれるので完備化できない．よって$X$は$D_1$-空間でない．
\end{exam}
\begin{defi}
$X = (X,\alpha)$を定義空間とする．ここで，ある$X^* = (X, \alpha^*)$が存在して
\begin{enumerate}
\item 任意の$x \in X$についてある$Y(x),Z(x) \subseteq X$が存在して$\alpha^*(x) = \bigcap_{y_i \in Y(x)} \alpha(y_i) \cap \bigcap_{z_j \in Z(x)} \alpha(z_j)^c$
\item 任意の$x \in X$について$\alpha^*(x) = \lk x \rk$
\end{enumerate}
となるとき，$X^*$を$X$の\textbf{$D_2$-完備化}($D_2$-completion)という．また，そのような$X^*$が存在するとき$X$を\textbf{$D_2$-定義空間}($D_2$-definition space)，あるいは単に\textbf{$D_2$-空間}という．
\end{defi}
\begin{exam}
$X$を$(\ref{e_d2})$の定義空間とする．$1 \in X$は完備であるが，その他の元は完備でない．ここで，
\begin{align*}
\alpha^*(1) &= \alpha(1) = \lk 1 \rk\\
\alpha^*(2) &= \alpha(2) \cap \alpha(1)^c = \lk 2 \rk\\
\alpha^*(3) &= \alpha(2)^c = \lk 3 \rk 
\end{align*}
とすると$X^* = (X, \alpha^*)$は完備となる．よって$X$は$D_2$-空間であり，その$D_2$-完備化は$X^*$である．
\end{exam}
\begin{exam}
定義より明らかに全ての$D_1$-空間は$D_2$-空間である．逆は必ずしも成り立たない．
\end{exam}
\subsection{正規部分空間とモンターニュ位相}
定義空間$X$とその類似系$\alpha$について，
\begin{enumerate}
\item $\alpha(\varnothing) = \varnothing$
\item $U \subseteq X$について$\alpha(U)=\bigcup_{x \in U} \alpha(x)$
\end{enumerate}
と矛盾なく定義できる．実際，$U \subseteq X$について類似系は$X$から$X$のべき集合への写像であるので実際は
\[
\alpha(U) = \lk \alpha(x) \middle| x \in U \rk
\]
となるが，上式の右辺においてその元である各類似から元を取り集合を為す操作は唯一つに定まるので，上のような定義が成り立つ．
\begin{prop}
定義空間$X = (X, \alpha)$と$X$の部分集合$U,V$について以下が成り立つ．
\begin{enumerate}
\item $\alpha(U \cap V) \subseteq \alpha(U) \cap \alpha(V)$
\item $\alpha(U \cup V) = \alpha(U) \cup \alpha(V)$
\end{enumerate}
\end{prop}
\Prf 省略
\begin{defi}
$X = (X, \alpha)$を定義空間，$U$を$X$の部分集合とする．ここで，$\alpha(U)=U$を満たすとき$U$を\textbf{正規部分集合}(normal subset)という．また，正規部分集合の誘導する部分定義空間$U = (U, \alpha\rst_U)$を\textbf{正規部分空間}(normal subspace)という．
\end{defi}
\begin{prop}
定義空間$X = (X, \alpha)$について，以下が成り立つ．
\begin{enumerate}
\item $\varnothing$，$X$は$X$の正規部分空間
\item $U,V$が$X$の正規部分空間ならば$U \cap V$は正規部分空間
\item $\lk U_i \rk$が$X$の正規部分空間ならば$\bigcup U_i$は正規部分空間
\end{enumerate}
\end{prop}
\Prf (1)明らか\\
(2)　$x\in \alpha(U \cap V) \Rightarrow x \in \alpha(U) \cap \alpha(V) \Rightarrow x \in U \cap V$より成り立つ．\\
(3)　$x \in \alpha(\bigcup U_i) = \bigcup \alpha(U_i) \Rightarrow x \in \bigcup U_i$より成り立つ．\Qed
\begin{defi}
定義空間は正規部分空間全体は位相空間をなす．これを\textbf{モンターニュ位相}(Montague topology)といい，その開集合系を$\mathcal{M}_{\alpha}$と表す．
\end{defi}
\begin{exam}
密着定義空間のモンターニュ位相は密着位相である．また，完備定義空間のモンターニュ位相は離散位相である．
\end{exam}
\begin{exam}
$X$を例\ref{e_d2}の定義空間とする．このとき$X$の正規部分集合は
\[
\lk \varnothing, \lk 1\rk, \lk 1,2 \rk, X\rk
\]
であり，これがモンターニュ位相の開集合系を為す．
\end{exam}
\begin{prop}
定義空間が$D_2$-空間であることと，その任意の正規部分空間が$D_2$-空間であることは同値である．
\end{prop}
\Prf
\begin{itemize}
\item $(\Leftarrow)$ 明らか．
\item $(\Rightarrow)$ $U \subseteq X$を$X$の正規部分空間とする．このとき，任意の$x \in U$についてある$X$の部分集合$Y(x),Z(x)$(ただし$x \in Y(x)$)が存在して
\[
\lk x \rk = \bigcap_{y \in Y(x)} \alpha(y) \cap \bigcap_{z \in Z(x)} \alpha(z)^c
\]
となる．ここで$U$の部分集合$Y'(x), Z'(x)$をそれぞれ
\begin{align*}
Y'(x) &= Y(x) \cap U\\
Z'(x) &= Z(x) \cap U
\end{align*}
とすると，

となるので，$(U, \alpha\rst_U)$は$D_2$-空間である．\Qed
\end{itemize}
\section{一般化定義空間}
前の章まで述べてきた定義空間とは，集合の点に類似点による部分集合を与えたものであったが，その類似度を$0$(類似)か$1$(非類似)かの2通りではなく，より広い値を取るものを考えたい．ここでその値域はある代数構造を持つものとすることで，命題の論理演算のように対象をその性質で\textit{演算}することが可能になる．具体的にはアーベル群，可換環および代数閉体などに値を取るものとして考える．この章ではまずアーベル群による一般化定義空間を定義し，その上で必要に応じて可換環や体による一般化を考察する．
\subsection{一般化定義空間}
\begin{defi}
集合$X$上のアーベル群$G$への\textbf{逆学的関数}(converse theoritic function)あるいは単に\textbf{関数}とは，写像$f:X \to G$のことである．集合$X$上の関数全体を$\Fun{X}$と表す．
\end{defi}
\begin{defi}
$X$上の\textbf{真関数系}(true function system)とは写像$\mathcal{F}:X \to \Fun{X}$のことである．ただし全ての$x\in X$に対して$(\mathcal{F}(x))(x)=e_X$を満たす．また，そのような$\mathcal{F}(x)$を$x$の\textbf{真関数}(true function)という．台集合$X$と真関数系$\mathcal{F}$による組$(X, \mathcal{F})$を\textbf{一般化定義空間}(generalized definition space)という．
\end{defi}
以降，この章では一般化定義空間を定義空間と呼称し，1章で定義した定義空間を\textit{二値定義空間}(binary definition space)という．前章までで取り扱った定義空間の類似系をアーベル群$\zz/2\zz$への真関数系と見ることが出来る: 集合の点に対して，その点において真となる関数を付与し，そうして与えられた関数で真になる自分以外の点を含めて類似をなしている．
\begin{defi}
$X = \{a\}$について，$\mathrm{id}(a)(a) = e_G$と定める．このとき$(X,\mathrm{id})$は定義空間となる．これを自明な定義空間という．また，$\mathrm{true}:X\to G$を$\mathrm{true}(x) = e_G$と定め，$\mathcal{T}(x) = \mathrm{true}$とする．このとき，$(X,\mathcal{T})$は定義空間となる．これを\textbf{密着定義空間}という．
\end{defi}
以下，特に断りのない限り$G \neq 0$とする．
\begin{defi}
定義空間$X = (X, \mathcal{F})$の元$x \in X$について，任意の$y\neq x$について$\mathcal{F}(x)(y) \neq e_G$であるとき，$x$は\textbf{完備}であるという．また，定義空間の任意の元が完備である時，その定義空間は\textbf{完備}であるという．
\end{defi}
\begin{exam}
$X=\{1,2,3\}, G=\zz$とする．$\mathcal{F}(a)(x) = a - x$と定めると，$(X, \mathcal{F})$は完備定義空間となる．
\end{exam}

\section{逆学的代数幾何学}
集合$X$になんらかの代数構造が入っていたとしても定義構造と代数構造は両立し，その代数構造に置ける二項演算は対象の演算として自然に定義空間へ導入される．つまり，対象の演算は対象を通じて定義されるが，同時に真関数に対しても演算が定義されることが分かる．つまり，定義空間においては現実の世界とwell-definedな形で命題を演算することが可能になる．これらの知識を用いて代数幾何学における交叉理論，接ベクトル空間，特異点などを考察する．

\subsection{射影空間上の定義空間}
$k$を代数閉体，$S = k\lkk x_0, \ldots, x_n \rkk$とし，斉次多項式$f \in S^h$について$V_Y(f) = \lk P \in Y \mid f(P) = 0\rk$とする．
\begin{defi}
$Y \subseteq \pp^n$とし，定義空間$Y=(Y,\alpha_Y)$の点$P \in Y$に対して$V_Y(f_P) = \alpha_Y(P)$となる$f_P \in S^h$が存在するとき，これを$P$の$Y$上の\textbf{モンターニュ多項式}という．
\end{defi}
任意の定義空間の点に対してモンターニュ多項式が存在するとは限らないが，$P$の類似が超曲面と$Y$の共通部分として表されるときはモンターニュ多項式が存在する．後により一般的に$\alpha_Y(P)$が単に代数的集合になる場合についても考えるが，以下はまずモンターニュ多項式が存在する状況を重点的に考察する．
\end{document}