# Introduction to Converse Theory
Author: Sid Montague  
Translator: dafuyafu   
 
This is an introductory textbook of converse theory.  
 
## Abstract
Converse theory is a branch of mathematics studying the relationship of elements in a set and conditions under which the converse of proposition is true. Modern converse theory is based on definition space theory. We first study about definition spaces. The main concepts of definition space are maximal complete component and principal instances which are important for classification and completion. We also study fundamentals of semiequivalence relation with which we discuss relations between elements integratedly and *super-locally*; more localy than with open set in topology, and we study classfication and completion of definition spaces with semiequivalence. Finally, we have the generalization of definition spaces, and we study application of converse theory to algebra.
 
## Contents

1. Definition space
1. Semiequivalence 
1. Classification of finite definition space
1. Completion
1. Generalized definition space
1. Algebraic converse theory

## Links

Blog: [愛想モルフィズム](http://dafuyafu.hatenablog.com)  
Twitter: [@dafuyafu](https://twitter.com/dafuyafu)  
 
## 訳者注

　本PDFはシド・モンターニュ著の「逆学入門」を日本語に訳したものである．各節ごとに訳を更新していく予定で，更新ごとに訳者のツイッターを通じてお知らせする．  
　モンターニュ氏はジャネーワ王国ヒラツカーナ出身の数学者で現在はジャネーワ王立科学アカデミー・カラジャカレッジ数学専攻の研究科長に就いている．彼がその学位論文の中で創始した逆学という理論は主に定義空間を構成し，有限定義空間を分類することが動機の一つであったが，現在では定義空間論から発展して代数学への応用が期待されている．  
　本書は第一章で定義空間論の大まかな概要や基本的な性質を取り上げる．以降は有限定義空間の分類を目標として，その準備のために半同値関係について述べ，位相空間との関係性を調べる．第四章で定義空間の完備化という操作を学習し，最後に現在の主要な研究分野である代数的逆学を紹介する．  
 
## Remarks
　この数学はフィクションであり、実際の数学とはなんら関係がありません。当リポジトリに対する査読・プルリクエストを随時募集しておりますが、概念そのものを否定する内容のものは、フィクションなのでご遠慮ください。  